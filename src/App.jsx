//@ts-check

import SongList from "./components/SongList";

import Amplify, { Auth } from "aws-amplify";
import awsconfig from "./aws-exports";
// import { AmplifySignOut, withAuthenticator } from "@aws-amplify/ui-react";
import { Switch, Route, BrowserRouter as Router, Link } from "react-router-dom";

import "./App.css";
import React, { useEffect, useState } from "react";
import { Button } from "@mui/material";
import SignIn from "./components/SignIn";

Amplify.configure(awsconfig);

function App() {
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    AssessLoggedInState();
  }, []);

  const AssessLoggedInState = () => {
    Auth.currentAuthenticatedUser()
      .then((session) => {
        console.log("logged in");
        setLoggedIn(true);
      })
      .catch(() => {
        console.log("not logged in");
        setLoggedIn(false);
      });
  };

  const signout = async () => {
    try {
      await Auth.signOut();
      setLoggedIn(false);
    } catch (error) {
      console.log("error", error);
    }
  };

  const onSignIn = () => {
    setLoggedIn(true);
  };

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <h2>My App Content</h2>
          {loggedIn ? (
            <Button
              variant="contained"
              color="primary"
              size="large"
              onClick={signout}
            >
              Sign Out
            </Button>
          ) : (
            <Link to="/signin" className="signIn-app">
              <Button variant="contained" color="success" size="large">
                Sign In
              </Button>
            </Link>
          )}
          {/* <AmplifySignOut /> */}
        </header>
        <Switch>
          <Route exact path="/">
            <SongList />
          </Route>
          <Route path="/signin">
            <SignIn onSignIn={onSignIn} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
