import React, { useState } from "react";

import { API, Storage } from "aws-amplify";
import { v4 as uuid } from "uuid";

import { createSong } from "../../graphql/mutations";

import PublishIcon from "@mui/icons-material/Publish";
import { IconButton, TextField } from "@mui/material";

const AddSong = ({ onUpload }) => {
  const [songData, setSongData] = useState({});
  const [mp3Data, setMp3Data] = useState();

  const uploadSong = async () => {
    const { key } = await Storage.put(`${uuid()}.mp3`, mp3Data, {
      contentType: "audio/mp3",
    });

    const { title, owner, description } = songData;

    const createSongInput = {
      id: uuid(),
      title,
      owner,
      description,
      filePath: key,
      likes: 0,
    };

    await API.graphql({
      query: createSong,
      variables: { input: createSongInput },
      authMode: "AWS_IAM",
    });

    onUpload();
  };

  return (
    <div className="newSong">
      <TextField
        label="Title"
        variant="standard"
        value={songData.title || ""}
        onChange={(e) => setSongData({ ...songData, title: e.target.value })}
      />
      <TextField
        label="Artist"
        variant="standard"
        value={songData.owner || ""}
        onChange={(e) => setSongData({ ...songData, owner: e.target.value })}
      />
      <TextField
        label="Description"
        variant="standard"
        value={songData.description || ""}
        onChange={(e) =>
          setSongData({ ...songData, description: e.target.value })
        }
      />
      <input
        type="file"
        accept="audio/mp3"
        onChange={(e) => setMp3Data(e.target.files[0])}
      />
      <IconButton onClick={uploadSong}>
        <PublishIcon />
      </IconButton>
    </div>
  );
};

export default AddSong;
