import React, { useEffect, useState } from "react";
import AddSong from "../AddSong";
import ReactPlayer from "react-player";
import { API, Storage } from "aws-amplify";

import { listSongs } from "../../graphql/queries";
import { updateSong } from "../../graphql/mutations";

import { IconButton, Paper } from "@mui/material";

import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import FavoriteIcon from "@mui/icons-material/Favorite";
import PauseIcon from "@mui/icons-material/Pause";
import AddIcon from "@mui/icons-material/Add";

const SongList = () => {
  const [songs, setSongs] = useState([]);
  const [songPlaying, setSongPlaying] = useState("");
  const [audioUrl, setAudioUrl] = useState("");
  const [showAddSong, setShowAddSong] = useState(false);

  useEffect(() => fetchSongs(), []);

  const toggleSong = async (index) => {
    if (songPlaying === index) {
      setSongPlaying("");
      return;
    }

    const songFilePath = songs[index].filePath;

    try {
      const fileAccessUrl = await Storage.get(songFilePath, { expires: 3600 });

      setSongPlaying(index);
      setAudioUrl(fileAccessUrl);

      return;
    } catch (error) {
      console.log("error", error);

      setSongPlaying("");
      setAudioUrl("");
    }
  };

  const fetchSongs = async () => {
    try {
      const songData = await API.graphql({
        query: listSongs,
        authMode: "AWS_IAM",
      });
      const songsList = songData.data.listSongs.items;
      setSongs(songsList);
    } catch (error) {
      console.log("error", error);
    }
  };

  const addLike = async (index) => {
    try {
      const song = songs[index];
      song.likes = song.likes + 1;
      delete song.createdAt;
      delete song.updatedAt;

      const songData = await API.graphql({
        query: updateSong,
        variables: { input: song },
        authMode: "AWS_IAM",
      });
      const songsList = [...songs];
      songsList[index] = songData.data.updateSong;

      setSongs(songsList);
    } catch (error) {
      console.log("error", error);
    }
  };

  return (
    <div className="songList">
      {songs.map((song, index) => {
        return (
          <Paper elevation={2} key={index}>
            <div className="songCard">
              <IconButton aria-label="play" onClick={() => toggleSong(index)}>
                {songPlaying === index ? <PauseIcon /> : <PlayArrowIcon />}
              </IconButton>
              <div>
                <div className="songTitle">{song.title}</div>
                <div className="songOwner">{song.owner}</div>
              </div>
              <div>
                <IconButton aria-label="like" onClick={() => addLike(index)}>
                  <FavoriteIcon />
                </IconButton>
                {song.likes}
              </div>
              <div className="songDescription">{song.description}</div>
            </div>
            {songPlaying === index ? (
              <div className="audioPlayer">
                <ReactPlayer
                  url={audioUrl}
                  controls
                  playing
                  height="50px"
                  onPause={() => toggleSong(index)}
                />
              </div>
            ) : null}
          </Paper>
        );
      })}
      {showAddSong ? (
        <AddSong
          onUpload={() => {
            setShowAddSong(false);
            fetchSongs();
          }}
        />
      ) : (
        <IconButton onClick={() => setShowAddSong(true)}>
          <AddIcon />
        </IconButton>
      )}
    </div>
  );
};

export default SongList;
